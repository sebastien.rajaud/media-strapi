import React, {useState} from 'react';
import {Link, useNavigate} from "react-router-dom";
import {IoMdAdd, IoMdSearch} from "react-icons/io";
import Avatar from "./Avatar";
import Modal from "./Pin/Modal";
import {useSelector} from "react-redux";

const Navbar = ({searchTerm, setSearchTerm}) => {

    const user = useSelector(state => state.userReducer.user)
    const navigate = useNavigate();
    const [isOpen, setIsOpen] = useState();

    const handleOpen = () => {
        setIsOpen(prevState => !prevState);
    }

    const handleChange = (e) => {
        setSearchTerm(e.target.value)
        if (e.target.value.length > 1) {
            navigate('/search')
        } else {
            navigate('/')
        }
    }

    if (!user) return null;

    return (
        <div className="flex gap-2 md:gap-5 w-full mt-5 pb-7 items-center">
            <div
                className="flex  justify-start items-center w-full px-2 rounded-md bg-white border-none outline-none focus-within:shadow-sm">
                <IoMdSearch fontSize={21} className="ml-1"/>
                <input
                    type="text"
                    onChange={handleChange}
                    placeholder="Search"
                    value={searchTerm}
                    // onFocus={() => navigate('search')}
                    className="p-2 w-full bg-white outline-none border-none"
                />
            </div>
            <div className="flex gap-3 items-center">
                <Link to={`/user-profile/${user?.id}`} className="hidden md:block">
                    <Avatar user={user} size={10}/>
                </Link>
                <button
                    onClick={handleOpen}
                    className="bg-indigo-500 text-white rounded-full w-9 h-9 md:w-14 md:h-11 flex justify-center items-center">
                    <IoMdAdd/>
                </button>
                {isOpen && <Modal isOpen={isOpen} setIsOpen={handleOpen}/>}
            </div>
        </div>
    );
}

export default Navbar;