import React, {useEffect, useState} from 'react';
import {AiFillEye} from "react-icons/ai";


const InputField = ({label, value, name, type = 'text', placeholder, switchType, onChange}) => {

    const [inputType, setInputType] = useState('text');
    let classes = "focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full sm:text-sm border-gray-300 mt-1"
    classes = switchType ? `${classes} rounded-l-md` : `${classes} rounded-md`

    useEffect(() => {
        setInputType(type)
    }, [type])

    const handleSwitch = () => {
        if (inputType === 'password') {
            setInputType('text')
        } else {
            setInputType('password')
        }
    }

    return (
        <>
            {label && (
                <label htmlFor={name} className="block text-sm font-medium text-gray-700">
                    {label}
                </label>
            )}
            <div className="flex ">
                <input
                    id={name}
                    name={name}
                    type={inputType}
                    autoComplete="on"
                    //required
                    className={classes}
                    placeholder={placeholder}
                    value={value ? value : null}
                    onChange={onChange}
                />
                {type === 'password' && switchType && (
                    <button type="button" onClick={handleSwitch} className="bg-indigo-400 p-2 mt-1 h-full rounded-r-md"
                            style={{height: '38px'}}><AiFillEye/>
                    </button>
                )}
            </div>
        </>
    );
};

export default InputField;