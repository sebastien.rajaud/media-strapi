import React from 'react';

const SelectField = ({label, name, options, value, onChange}) => {
    return (
        <>
            <label htmlFor="category" className="block text-sm font-medium text-gray-700">
                {label}
            </label>
            <select
                id={name}
                name={name}
                className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            >
                {options && options.map(opt => (
                    <option key={opt._id}
                            className="text-base border-0 outline-none capitalize bg-white text-black"
                            value={opt._id}
                    >{opt.name}</option>
                ))}
            </select>
        </>
    );
};

export default SelectField;