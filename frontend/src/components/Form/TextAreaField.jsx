import React from 'react';

const TextAreaField = ({label, name, placeholder, value, onChange}) => {
    return (
        <>
            <label htmlFor="about" className="block text-sm font-medium text-gray-700">
                {label}
            </label>
            <textarea
                rows={5}
                id={name}
                name={name}
                placeholder={placeholder}
                className="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300 mt-1"
            />
        </>
    );
};

export default TextAreaField;