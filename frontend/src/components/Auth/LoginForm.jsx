import React, {useState} from 'react';

import {HiLockClosed} from "react-icons/hi";
import {useSelector} from "react-redux";
import InputField from "../Form/InputField";

const LoginForm = ({handleSubmit}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const alert = useSelector(state => state.userReducer.alert)

    const onSubmit = async (e) => {
        e.preventDefault();
        const isValid = await handleSubmit(email, password);
        if (isValid) {
            setEmail('');
            setPassword('');
        }
    }

    return (
        <form className="mt-8 space-y-6" onSubmit={onSubmit}>
            <div className="rounded-md shadow-sm -space-y-px gap-2 flex flex-col">
                <div>
                    <InputField label={null} type="email" name="email" placeholder="Adresse Email"
                                onChange={e => setEmail(e.target.value)} value={email}/>
                </div>
                <div>
                    <InputField label={null} switchType type="password" name="password" placeholder="Password"
                                onChange={e => setPassword(e.target.value)} value={password}/>
                </div>
            </div>
            {alert.active && (
                <div className="text-red-500 mt-2 text-center">{alert.message}</div>
            )}
            <div>
                <button
                    type="submit"
                    className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                    <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                      <HiLockClosed className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                                    aria-hidden="true"/>
                    </span>
                    Sign in
                </button>
            </div>
        </form>
    );
}

export default LoginForm;