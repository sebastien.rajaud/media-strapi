import React from 'react';
import Loader from 'react-loader-spinner';

const Spinner = ({message, type = 'BallTriangle', height = 50, width = 200, color = '#4f46e4', row=false, className}) => {
    return (
        <div className={`flex ${!row && 'flex-col'} justify-center items-center w-full h-full`}>
            <Loader
                type={type}
                color={color}
                height={height}
                width={width}
                className={`m-5 ${className}`}
            />
            <p className="text-lg text-center px-2">{message}</p>
        </div>
    );
};

export default Spinner;