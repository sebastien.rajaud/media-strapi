import React from 'react';
import Avatar from "../Avatar";
import {getFullname} from "../../utils/user";
import moment from "moment";

const Comment = ({data}) => {
    const {postedBy, comment} = data;

    return (
        <div className="flex rounded-md bg-white p-4 gap-4 w-full">
            <Avatar user={postedBy} size={8} />
            <div className="text-xs text-gray-600 font-light ">
                <h3 className="mb-1"><span className="font-bold ">{getFullname(postedBy)}</span> le {moment(data.createdAt).format('DD/MM/Y à HH:MM:ss')}</h3>
                {comment}
            </div>
        </div>
    );
};

export default Comment;