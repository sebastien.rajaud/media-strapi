import React from 'react';
import {getMediaUrl} from "../utils/media";

const Avatar = ({user, size=8, className}) => {

    return (
        <div className={`w-${size} h-${size} rounded-full bg-indigo-500 overflow-hidden ${className} `} style={{aspectRatio: '1/1'}} >
            <div className="flex flex-col items-center justify-center h-full w-full" style={{aspectRatio: '1/1'}} >
                {user?.avatar ? (
                    <img
                        className="object-cover w-full h-full"
                        src={getMediaUrl(user.avatar?.url)}
                        alt="user-profile"
                    />
                ) : (
                    <span className="text-white">
                    {`${user.firstname[0]}${user.lastname[0]}`}
                </span>
                )}
            </div>

        </div>
    );
}

export default Avatar;