import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import moment from "moment";
import {IoMailOutline} from "react-icons/io5";
import {AiOutlineLogout} from "react-icons/ai";
import {FaUserEdit} from "react-icons/fa";

import Spinner from "../Spinner";
import {fetchlogout} from "../../api/apiUser";
import {getFullname} from "../../utils/user";
import Avatar from "../Avatar";
import {fetchPinsCreatesByUser, fetchPinsSavesByUser} from "../../actions/profileActions";
import MasonryLayout from "../Pin/MasonryLayout";
import Navbar from "../Navbar";
import EditUserForm from "./EditUserForm";
import {fetchProfile} from "../../actions/profileActions";

const activeStyle = "mr-2 inline-block py-4 px-4 text-sm font-medium text-center text-blue-600 rounded-t-lg border-b-2 border-blue-600 active dark:text-indigo-500 dark:border-indigo-500";
const unactiveStyle = "mr-2 inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300";

const UserProfile = (props) => {

    const dispatch = useDispatch()

    const user = useSelector(state => state.userReducer.user);
    const profile = useSelector(state => state.profileReducer.user);
    const pins = useSelector(state => state.profileReducer.pins);
    const isLoading = useSelector(state => state.profileReducer.isLoading);

    const {userId} = useParams();

    const [activeTab, setActiveTab] = useState('created');
    const [searchTerm, setSearchTerm] = useState('');
    const [editUser, setEditUser] = useState(false);

    const navigate = useNavigate();

    const getProfile = () => {
        dispatch(fetchProfile(userId))
    }

    useEffect(() => {
        if (user) {
            getProfile()
        }
    }, [user, userId])

    useEffect(() => {
        if (profile) {
            dispatch(fetchPinsCreatesByUser(profile.id))
        }
    }, [profile, dispatch])

    useEffect(() => {
        getPins();
    }, [profile, activeTab])

    const getPins = async () => {
        if (!profile) return false;
        if (activeTab === 'saved') {
            dispatch(fetchPinsSavesByUser(profile.id))
        } else if (activeTab === 'created') {
            dispatch(fetchPinsCreatesByUser(profile.id))
        }

    }

    const handleTab = (e) => {

        const data = e.target.dataset.tab;
        setActiveTab(data);

    }

    if (!profile || !user) return <Spinner message="User is loading..."/>

    return (
        <div className="px-2 md:px-5">
            <div className="bg-gray-50">
                <Navbar searchTerm={searchTerm} setSearchTerm={setSearchTerm} user={user}/>
            </div>
            <div className="relative">

                <div className="relative -mx-4 top-0 pt-[25%] overflow-hidden bg-indigo-700 ">
                    <img
                        className="absolute inset-0 object-cover object-top w-full h-full filter mix-blend-screen grayscale"
                        src="https://source.unsplash.com/1600x900/?textures-patterns,nature"
                        alt=""/>
                </div>

                <div className="flex flex-col justify-center items-center">
                    {editUser ? (
                        <EditUserForm user={profile} setEditUser={setEditUser}/>
                    ) : (
                        <>
                            <Avatar
                                user={profile}
                                size={16}
                                className="-mt-10 z-40 relative shadow-xl "
                            />
                            <h1 className="font-bold text-3xl text-center mt-3">
                                {getFullname(profile)}
                            </h1>
                            <p className="text-sm font-light text-gray-400">Compte créé
                                le {moment(profile.createdAt).format('DD/MM/YY')}</p>

                            <div className="flex gap-4 my-2">
                                <a href={`mailto:${profile.email}`} className="block my-2">
                                    <IoMailOutline size={30} className="text-indigo-500"/>
                                </a>
                                {profile.id === user.id && (

                                    <button
                                        type="button"
                                        onClick={() => setEditUser(true)}
                                    >
                                        <FaUserEdit className="text-indigo-500" fontSize={30}/>
                                    </button>
                                )}
                            </div>
                        </>

                    )}


                    {profile.id === user.id && (
                        <div className="absolute top-0 z-1 right-0 p-2">
                            <button
                                type="button"
                                className="bg-white p-2 rounded-full cursor-pointer outline-none shadow-md"
                                onClick={() => dispatch(fetchlogout(navigate))}
                            >
                                <AiOutlineLogout color="red" fontSize={21}/>
                            </button>

                        </div>
                    )}
                </div>
                <div className="border-b border-gray-200 ">
                    <ul className="flex flex-wrap -mb-px justify-center">
                        <li
                            onClick={handleTab}
                            data-tab="created"
                            className={activeTab === "created" ? activeStyle : unactiveStyle}>
                            Created
                        </li>
                        <li
                            onClick={handleTab}
                            data-tab="saved"
                            className={activeTab === "saved" ? activeStyle : unactiveStyle}>
                            Saved
                        </li>
                    </ul>
                </div>
                <div className="mt-4">
                    {isLoading ? (
                        <Spinner message="Loading pins..."/>
                    ) : pins && <MasonryLayout pins={pins}/>}
                </div>
            </div>
        </div>
    );
}

export default UserProfile;