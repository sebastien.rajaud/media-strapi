import React, {useEffect, useRef, useState} from 'react';
import Avatar from "../Avatar";
import {GiCancel} from "react-icons/gi";
import {BsSave2Fill} from "react-icons/bs";
import {fetchUpdateUser, fetchUpdateAvatarUser} from "../../actions/profileActions";
import {uploadImage} from "../../utils/media";
import {useDispatch} from "react-redux";
import Spinner from "../Spinner";
import InputField from "../Form/InputField";


const EditUserForm = ({user, setEditUser}) => {
    const dispatch = useDispatch();

    const [isLoading, setIsLoading] = useState(false);

    const [preview, setPreview] = useState(null);
    const [selectedFile, setSelectedFile] = useState();
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const formRef = useRef();
    const fileRef = useRef();

    useEffect(() => {
        setFirstName(user.firstname);
        setLastName(user.lastname);
    }, [user])


    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);
        const formData = new FormData();

        let obj = {
            field: "avatar",
            source: 'users-permissions',
            ref: 'user',
            refId: user.id,
        }

        let fileUpload = null;
        const updateObject = {
            firstname,
            lastname
        };
        if (selectedFile) {
            formData.append('data', JSON.stringify(obj))
            formData.append(`files`, selectedFile, selectedFile.name);

            fileUpload = await dispatch(fetchUpdateAvatarUser(formData));
            updateObject.avatar = fileUpload[0]._id ? fileUpload[0]._id : null;
        }

        dispatch(fetchUpdateUser(user.id, updateObject));

        setIsLoading(false);
        setEditUser(false);

    }

    const handleUploadImage = (e) => {
        uploadImage(e, setPreview)
        setSelectedFile(fileRef.current.files[0]);
    }

    const handleClose = () => {
        setEditUser(false);
        setPreview(null);
    }

    return (
        <div className="w-full mt-5 drop-shadow-2xl">
            {isLoading ? <Spinner/> : (

                <form ref={formRef} onSubmit={handleSubmit}
                      className="flex w-full items-center justify-center h-full bg-white lg:p-5 p-3 gap-4">


                    <div className="flex flex-col justify-end items-end">

                        <label className="flex flex-col justify-end items-end">
                            {preview ? (
                                <img src={preview} className="rounded-full bg-indigo-500 overflow-hidden w-16 h-16" alt="avatar"/>
                            ) : (
                                <Avatar user={user} size={16} className="cursor-pointer"/>
                            )}
                            <input
                                type="file"
                                name="avatar"
                                onChange={handleUploadImage}
                                className="w-0 h-0"
                                ref={fileRef}
                            />
                        </label>

                    </div>

                    <div className="flex items-end gap-6 ">

                        <div>
                            <InputField placeholder="Jon" label="Prénom" name="firstname" value={firstname} onChange={(e) => setFirstName(e.target.value)} />
                        </div>

                        <div>
                            <InputField placeholder="Snow" label="Nom" name="lastname" value={lastname} onChange={(e) => setLastName(e.target.value)} />
                        </div>

                        <button
                            type="submit"
                            className=" flex items-center text-indigo-500 gap-2 border-1 rounded-md py-1 px-3 border-indigo-500 font-bold outline-none"
                            style={{height: '38px'}}
                            // disabled={!preview}
                        >
                            <BsSave2Fill color="#6366f1" size={20}/> Save
                        </button>
                        <button
                            type="button"
                            className=" absolute top-2 right-2 flex self-start gap-2 outline-none"
                            onClick={handleClose}
                        >
                            <GiCancel size={20}/>
                        </button>
                    </div>
                </form>
            )}
        </div>
    );
};

export default EditUserForm;