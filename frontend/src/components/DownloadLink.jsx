import React from 'react';
import {getMediaUrl, handleDownload} from "../utils/media";
import {MdDownloadForOffline} from "react-icons/md";


const DownloadLink = ({image}) => {

    return (
        <div
            onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                handleDownload(getMediaUrl(image?.url), image.name);
            }}
            className="bg-indigo-500 w-8 h-8 rounded-full flex items-center justify-center text-white text-xl opacity-75 hover:opacity-100 hover:shadow-md outline-none cursor-pointer"
        >
            <MdDownloadForOffline/>
        </div>
    );
};

export default DownloadLink;