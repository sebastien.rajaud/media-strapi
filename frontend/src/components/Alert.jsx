import React, {useEffect, useState} from 'react';

const Alert = ({message, type, delay = 3000}) => {

    const [display, setDisplay] = useState(true);

    const color = {
        error: 'red',
        warning: 'orange',
        success: 'green',
    }


    useEffect(() => {
        const timer = setTimeout(function () {
            setDisplay(false)
        }, delay);

        return () => clearTimeout(timer)
    }, [delay])

    if (!display) return null;

    return (
        <div onClick={() => setDisplay(false)} className="absolute bottom-2 right-0 left-0 text-center">
            <div className="p-2">
                <div
                    className={`inline-flex items-center bg-white leading-none text-${color[type]}-700 rounded-full p-2 shadow text-teal text-sm`}>
                    <span
                        className={`inline-flex bg-${color[type]}-700 text-white rounded-full h-6 px-3 justify-center items-center`}>Pink</span>
                    <span className="inline-flex px-2">{message}</span>
                </div>
            </div>
        </div>
    );
};

export default Alert;