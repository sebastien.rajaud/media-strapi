import React, {useEffect} from 'react';

import MasonryLayout from "../Pin/MasonryLayout";
import {fetchSearch} from "../../actions/searchActions";
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../Spinner";

function Search({searchTerm}) {

    const dispatch = useDispatch();
    const pins = useSelector(state => state.searchReducer.pins)
    const isLoading = useSelector(state => state.searchReducer.isLoading)

    useEffect( () => {
            dispatch(fetchSearch(searchTerm));


    }, [searchTerm, dispatch])

    return (
        <div>
            <div className="mt-10 text-center text-xl">
                {isLoading ? <Spinner/> : (
                    <MasonryLayout pins={pins}/>
                )}
            </div>

        </div>
    );
}

export default Search;