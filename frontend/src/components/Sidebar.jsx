import React, {useEffect} from 'react';
import {NavLink, Link, useNavigate} from "react-router-dom";
import {getMediaUrl} from "../utils/media";
import {useDispatch, useSelector} from "react-redux";
import {logout} from '../actions/userActions';
import {fetchPinCategories} from "../actions/pinActions";


const Sidebar = ({closeToggle, user}) => {

    const categories = useSelector(state => state.pinReducer.categories);
    const navigate = useNavigate();
    const dispatch = useDispatch();


    const handlCloseSidebar = () => {
        if (closeToggle) {
            closeToggle(false);
        }
    }


    useEffect(() => {
        if (user) {
            dispatch(fetchPinCategories())
        }
    }, [user, dispatch])

    return (
        <div className="flex flex-col justify-between bg-white h-full overflow-y-scrikk min-w-210 hide-scrollbar">
            <nav className="rounded-md w-72 h-screen flex-col justify-between">
                <div className="bg-white h-full">
                    <div className="flex justify-center py-10 shadow-sm pr-4">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-14 w-14 text-indigo-600" fill="none"
                             viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                  d="M9 3v2m6-2v2M9 19v2m6-2v2M5 9H3m2 6H3m18-6h-2m2 6h-2M7 19h10a2 2 0 002-2V7a2 2 0 00-2-2H7a2 2 0 00-2 2v10a2 2 0 002 2zM9 9h6v6H9V9z"/>
                        </svg>
                        <div className="pl-2">
                            <Link
                                to="/"
                                onClick={handlCloseSidebar}
                            >
                                <h1 className="text-2xl font-bold text-indigo-600">VENUS</h1>
                                <span className="text-xs block text-gray-800">DASHBOARD</span>
                            </Link>
                        </div>
                    </div>
                    <div className="pl-10">
                        <ul className="space-y-8 pt-10">
                            <li>
                                <NavLink
                                    to="/"
                                    className="flex space-x-4 items-center hover:text-indigo-600 cursor-pointer transition-all duration-200 ease-in-out capitalize"
                                    onClick={handlCloseSidebar}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                              d="M9 17V7m0 10a2 2 0 01-2 2H5a2 2 0 01-2-2V7a2 2 0 012-2h2a2 2 0 012 2m0 10a2 2 0 002 2h2a2 2 0 002-2M9 7a2 2 0 012-2h2a2 2 0 012 2m0 10V7m0 10a2 2 0 002 2h2a2 2 0 002-2V7a2 2 0 00-2-2h-2a2 2 0 00-2 2"/>
                                    </svg>
                                    <span className="ml-4">
                                        Home
                                    </span>
                                </NavLink>
                            </li>
                            {user && (
                                <li>
                                    <NavLink
                                        to={`user-profile/${user.id}`}
                                        className="flex space-x-4 items-center hover:text-indigo-600 cursor-pointer transition-all duration-200 ease-in-out capitalize"
                                        onClick={handlCloseSidebar}
                                    >
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24"
                                             stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                  d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"/>
                                        </svg>
                                        <span className="ml-4">Account</span>
                                    </NavLink>
                                </li>

                            )}
                            {categories && categories.length && categories.map(category => (
                                <li key={category.name}>
                                    <NavLink
                                        to={`/category/${category.slug}`}
                                        className="flex space-x-4 items-center hover:text-indigo-600 cursor-pointer transition-all duration-200 ease-in-out capitalize"
                                        key={category.name}
                                        onClick={handlCloseSidebar}
                                    >
                                        <img src={getMediaUrl(category.image.url)} alt={category.name}/>
                                        <span className="ml-4">{category.name}</span>
                                    </NavLink>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </nav>
            {user && (

                <div
                    className="bg-white flex items-center space-x-4 pl-10 pb-10 hover:text-indigo-600 cursor-pointer">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24"
                         stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                              d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"/>
                    </svg>
                    <button
                        type="button"
                        onClick={() => dispatch(logout(navigate))}
                    >
                        Logout
                    </button>
                </div>
            )}
        </div>
    )
}

export default Sidebar;