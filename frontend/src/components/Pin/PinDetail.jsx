import React, {useEffect} from 'react';
import {useParams, Link} from "react-router-dom";
import Spinner from "../Spinner";
import {getMediaUrl} from "../../utils/media";
import DownloadLink from "../DownloadLink";
import {getFullname} from "../../utils/user";
import Avatar from "../Avatar";
import moment from 'moment';
import Comments from "../../container/Comments";
import {useDispatch, useSelector} from "react-redux";
import {fetchPinDetail} from "../../actions/pinActions";

const PinDetail = () => {
    const dispatch = useDispatch();
    const {pinId} = useParams();
    const pin = useSelector(state => state.pinReducer.pinDetail)
    const isLoading = useSelector(state => state.pinReducer.isLoading)

    useEffect( () => {
        dispatch(fetchPinDetail(pinId))

    }, [dispatch, pinId])



    if (!pin || isLoading) return <Spinner message="Pin loading"/>;

    return (
        <>
            <div className="relative -mx-4 top-0 pt-[25%] overflow-hidden bg-indigo-700 ">
                <img
                    className="absolute inset-0 object-cover object-top w-full h-full filter mix-blend-screen grayscale"
                    src="https://source.unsplash.com/1600x900/?textures-patterns,nature"
                    alt=""/>
            </div>
            <div className="mt-[-10%]  sm:w-full  lg:w-4/5 xl:w-1/2 mx-auto drop-shadow-2xl">
                <div className="relative pt-[56.25%] overflow-hidden rounded-2xl">
                    <img src={pin?.image && getMediaUrl(pin?.image?.url)}
                         alt="user-post"
                         className="w-full h-full absolute inset-0 object-cover"
                    />
                </div>

                <div className="mt-5 flex justify-end">
                    <DownloadLink image={pin.image}/>
                </div>
            </div>
            <div className="mx-auto py-8  sm:w-full  lg:w-4/5 xl:w-1/2">
                <article>
                    <div className="text-sm mb-1 text-gray-400 flex items-center font-light gap-2">Posted
                        on {moment(pin.createdAt).format('DD/MM/Y')} by <Avatar user={pin.postedBy}
                                                                                size={5}/> {getFullname(pin.postedBy)}
                    </div>
                    <h1 className="text-2xl font-bold">{pin.title}</h1>
                    {pin.category && (
                        <>
                            <h2 className="mt-2 text-sm text-gray-500">
                                Category :&nbsp;
                                <Link
                                    to={`/category/${pin.category?.slug}`}
                                    className="text-indigo-500"
                                >{pin.category?.name}</Link>
                            </h2>
                            <p className="mt-3 text-base">{pin.about}</p>
                        </>
                    )}
                </article>
                <div className="border-b-1 border-gray-300 my-10 w-1/2 mx-auto"></div>
                <Comments id={pinId} />

            </div>
        </>
    );
}

export default PinDetail;