import React, {useState, useRef} from 'react';
import {AiOutlineCloudUpload} from "react-icons/ai";
import Alert from "../Alert";
import {useNavigate} from "react-router-dom";
import {generateFormData} from "../../utils/forms";
import {useDispatch, useSelector} from "react-redux";
import {fetchCreatePin} from "../../actions/pinActions";
import {WARNING} from "../../actions/constants";
import InputField from "../Form/InputField";
import SelectField from "../Form/SelectField";
import TextAreaField from "../Form/TextAreaField";


const CreatePinForm = ({onSubmitAction}) => {

    const dispatch = useDispatch();
    const user = useSelector(state => state.userReducer.user);
    const categories = useSelector(state => state.pinReducer.categories)
    const isLoading = useSelector(state => state.pinReducer.isLoading)

    const formRef = useRef();
    const [preview, setPreview] = useState(null);

    const [error, setError] = useState({
        isError: false,
        type: '',
        message: ''
    });

    const navigate = useNavigate();

    const uploadImage = (e) => {

        const file = e.target.files[0];
        if (
            file && (
                file.type === 'image/jpeg' ||
                file.type === 'image/png' ||
                file.type === 'image/gif' ||
                file.type === 'image/tiff' ||
                file.type === 'image/svg'
            )
        ) {
            const fileReader = new FileReader();

            fileReader.onload = () => {
                setPreview(fileReader.result)
            }
            fileReader.readAsDataURL(file);


        } else {
            setError({
                isError: true,
                type: WARNING,
                message: 'Format available for images : jpeg, png, tiff, svg'
            })
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formElements = formRef.current.elements;
        const formData = generateFormData(formElements);

        try {
            dispatch(fetchCreatePin(formData));
            onSubmitAction();
            navigate('/');

        } catch (e) {
            console.log(e)
        }

    }

    return (
        <div className="flex flex-col justify-center items-center mt-5 h-full ">
            <form ref={formRef}
                  onSubmit={handleSubmit}
                  className="flex w-full flex-col justify-center h-full items-center bg-white lg:p-5 p-3 ">

                <div className="bg-secondaryColor p-3 flex  w-full h-200">

                    <div
                        className="flex justify-center items-center flex-col border-2 border-dotted border-gray-300 p-3 w-full h-full">
                        {preview && <img src={preview} className="object-cover h-full w-full" alt="upload media"/>}
                        <label className={preview ? 'h-0' : 'h-full'}>
                            {!preview && (

                                <div className="flex flex-col items-center justify-center h-full text-center">
                                    <div className="flex flex-col justify-center items-center">
                                        <p className="font-bold text-2xl">
                                            <AiOutlineCloudUpload/>
                                        </p>
                                        <p className="text-md">Click to upload</p>
                                    </div>
                                    <p className="mt-2 text-sm text-gray-400">
                                        Use high quality JPG, SVG, PNG, GIF less than 20 MB
                                    </p>
                                </div>
                            )}
                            <input
                                type="file"
                                name="image"
                                onChange={uploadImage}
                                className="w-0 h-0"
                            />
                        </label>
                    </div>
                </div>
                <div className="flex flex-1 flex-col gap-6 mt-5 w-full">
                    <div>
                        <InputField label="Titre" name="title" placeholder="Renseignez votre titre"/>
                    </div>

                    <div>
                        <SelectField label="Catégories" options={categories} name="category"/>
                    </div>
                    <div>
                        <TextAreaField label="A propos" name="about" placeholder="Descriptif de votre photo"/>
                    </div>
                    <input type="hidden" name="postedBy" value={user.id}/>
                    <div className="flex justify-end items-end mt-5">
                        <button
                            type="button"
                            className="bg-indigo-500 text-white font-bold p-2 rounded-md w-28 outline-none"
                            onClick={handleSubmit}
                            disabled={!preview || isLoading}
                        >
                            Save Pin
                        </button>
                    </div>
                </div>
            </form>
            {error.isError && <Alert type={error.type} message={error.message}/>}
        </div>
    );
}

export default CreatePinForm;