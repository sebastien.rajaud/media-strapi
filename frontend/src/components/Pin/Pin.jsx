import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from "react-router-dom";
import {getMediaUrl} from "../../utils/media";
import Avatar from "../Avatar";
import {AiTwotoneDelete} from "react-icons/ai";
import {fetchDeletePin, fetchLikePin, fetchUnlike, fetchUnlikePin} from "../../actions/pinActions";
import Loader from "react-loader-spinner";
import {AiOutlineHeart, AiTwotoneHeart} from "react-icons/ai";
import DownloadLink from "../DownloadLink";
import {useDispatch, useSelector} from "react-redux";

const Pin = ({pin}) => {
    const {postedBy, image, id, saves} = pin;
    const [postHovered, setPostHovered] = useState(false);

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const user = useSelector(state => state.userReducer.user);
    const [isLoading, setIsLoading] = useState(false);

    const [alreadySaved, setAlreadySaved] = useState(false);

    useEffect(() => {
        if (user) {
            setAlreadySaved(!!saves?.filter(item => item.id === user.id)?.length)
        }
    }, [user, saves])

    const likePin = async (id) => {
        if (user) {
            setIsLoading(true);
            let data = null;
            if (alreadySaved) {
                data = await dispatch(fetchUnlikePin(id, user.id))
            } else {
                data = await dispatch(fetchLikePin(id, user.id))
            }

            if (data) {
                setAlreadySaved(state => !state);
            }

            setIsLoading(false);
        }

    }

    const deletePin = (id) => {
        dispatch(fetchDeletePin(id))
    }

    if (!pin) return null;

    return (
        <div className="m-2">
            <div
                className="relative cursor-zoom-in w-auto hover:shadow-lg overflow-hidden transition-all duration-500 ease-in-out"
                onClick={() => navigate(`/pin-detail/${id}`)}
                onMouseEnter={(e) => setPostHovered(true)}
                onMouseLeave={(e) => setPostHovered(false)}
            >
                <img className="rounded-md w-full" alt="user-post"
                     src={getMediaUrl(image?.formats?.small?.url)}/>

                {postHovered && (
                    <div
                        className="absolute top-0 w-full h-fill flex flex-col justify-between p-1 pr-2 pt-2 pb-2 z-50"
                        style={{height: '100%'}}
                    >
                        <div className="flex items-center justify-between">

                            <div className="flex gap-2">
                                <DownloadLink image={image}/>
                            </div>

                            <button
                                type="button"
                                className="w-10 h-8 rounded-full flex gap-1 items-center justify-center bg-indigo-500 opacity-70 hover:opacity-100 text-white font-bold text-sm rounded-3xl hover:shadow-md outline-none"
                                onClick={e => {
                                    e.stopPropagation();
                                    likePin(id, user.id);
                                }}
                            >
                                {isLoading ? (
                                    <Loader
                                        type="Oval"
                                        color="#ffffff"
                                        height={20}
                                        width={20}
                                    />
                                ) : (
                                    alreadySaved ? (
                                        <><AiTwotoneHeart width={5} height={5}/> {saves.length}</>
                                    ) : (
                                        <><AiOutlineHeart width={5} height={5}/> {saves.length}</>

                                    )
                                )}


                            </button>

                        </div>
                        <div className="flex justify-between items-center gap-2 w-full">
                            {postedBy.id === user.id && (
                                <button
                                    type="button"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        deletePin(id)
                                    }}
                                    className="bg-indigo-500 p-2 opacity-70 hover:opacity-100 font-bold text-white text-base rounded-3xl hover:shadow-md outline-none"
                                >
                                    <AiTwotoneDelete/>
                                </button>
                            )}
                        </div>
                    </div>
                )}


            </div>
            <Link
                to={`/user-profile/${postedBy?.id}`}
                className="flex items-center gap-2 mt-2"
            >
                <Avatar user={postedBy}/>
                <p className="font-semibold capitalize">{postedBy?.firstname} {postedBy?.lastname}</p>
            </Link>
        </div>
    );
}

export default Pin;