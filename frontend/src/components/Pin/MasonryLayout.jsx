import React from 'react';
import Masonry from 'react-masonry-css';
import Pin from './Pin';



const breakPointObj = {
    default: 4,
    3000: 5,
    2000: 3,
    1200: 3,
    1000: 2,
    500: 1

}

const MasonryLayout = ({pins}) => {
    if (!pins.length) return <p className="text-center text-base font-bold">Il n'y a pas de résultat</p>

    return (
        <>
            <Masonry className="flex animate-slide-fwd lg:w-4/5 mx-auto" breakpointCols={breakPointObj}>
                {pins?.map(pin => <Pin key={pin.id} pin={pin} className="w-max"/>)}
            </Masonry>

        </>

    )
}
export default MasonryLayout;