import React, {useEffect, useState} from 'react';
import MasonryLayout from "../Pin/MasonryLayout";
import Spinner from "../Spinner";
import {useNavigate, useParams} from "react-router-dom";
import Alert from "../Alert";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchAllPins,
    fetchAllPinsLoadMore,
    fetchPinsByCategory,
    fetchPinsByCategoryLoadMore
} from "../../actions/pinActions";
import {LIMIT} from "../../api";

const Feed = () => {

    const dispatch = useDispatch();
    const [start, setStart] = useState(LIMIT + 1)
    const [isLoadMore, setIsLoadMore] = useState(false)
    const navigate = useNavigate();

    const pins = useSelector(state => state.pinReducer.pins);
    const isLoading = useSelector(state => state.pinReducer.isLoading);
    const alert = useSelector(state => state.pinReducer.alert);
    const totalPinsCount = useSelector(state => state.pinReducer.totalPinsCount);
    const {slug} = useParams();


    const handleLoadMore = async (e) => {
        setStart(prevState => prevState + LIMIT)
        setIsLoadMore(true)
        if (!slug) {
            await dispatch(fetchAllPinsLoadMore(start))
        } else {
            await dispatch(fetchPinsByCategoryLoadMore(slug, start))
        }
        setIsLoadMore(false);
    }

    useEffect(() => {
        setStart(LIMIT );
    }, [navigate])

    useEffect(() => {
        if (!slug) {
            dispatch(fetchAllPins());
        } else {
            dispatch(fetchPinsByCategory(slug));
        }
    }, [slug, dispatch]);

    if (isLoading) return <Spinner message="We're loading pins..."/>;

    return (
        <div>
            {pins && pins?.length > 0 && (
                <>
                    <MasonryLayout pins={pins} length={pins.length} />
                    {totalPinsCount > pins.length && (
                        <div className="flex justify-center mt-12">
                            <button className="flex justify-center bg-indigo-500 text-white font-bold p-2 rounded-md w-60 outline-none"
                                    onClick={handleLoadMore}
                                disabled={isLoadMore}
                            >{
                                isLoadMore ? (
                                    <Spinner message="Chargement" type="Oval" width={20} height={20} color="#ffffff" row className="m-0 mr-2"/>
                                ) : (
                                   <> Load more</>
                                )
                            }</button>
                        </div>
                    )
                       }
                </>
            )}
            {alert.active && <Alert type={alert.type} message={alert.message}/>}
        </div>
    );
}

export default Feed;