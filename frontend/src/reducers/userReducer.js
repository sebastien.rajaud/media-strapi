import {
    ERROR, FETCH_UPDATE_USER_SUCCESS, HYDRATE_USER, HYDRATE_USER_ERROR, HYDRATE_USER_SUCCESS,
    LOGIN,
    LOGIN_ERROR,
    LOGIN_SUCCESS,
    LOGOUT,
    LOGOUT_ERROR,
    LOGOUT_SUCCESS,
    SUCCESS
} from "../actions/constants";

const initialState = {
    user: null,
    isAuthenticated: false,
    isLoading: false,
    alert: {
        active: false,
        type: '',
        message: ''
    }
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN :
        case LOGOUT:
        case HYDRATE_USER:
            return {
                ...state,
                isLoading: true,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            };
        case LOGIN_SUCCESS :

            return {
                ...state,
                user: action.data,
                isAuthenticated: true,
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Bienvenue sur votre espace Médiathèque'
                }
            };

        case LOGIN_ERROR :
            return {
                ...state,
                user: null,
                isAuthenticated: false,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: action?.error
                }
            };

        case LOGOUT_SUCCESS :
            return {
                ...state,
                user: null,
                isAuthenticated: false,
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            };
        case LOGOUT_ERROR :
            return {
                ...state,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: action.error
                }
            }


        case HYDRATE_USER_SUCCESS :
            return {
                ...state,
                user: action.data,
                isAuthenticated: true,
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_UPDATE_USER_SUCCESS :
            return {
                ...state,
                user: action.data,
                isLoading: false,
                alert: {...initialState.alert}
            }

        case HYDRATE_USER_ERROR :
            return {
                ...state,
                isAuthenticated: false,
                user: null,
            };

        default:
            return state;
    }
}

export default userReducer;
