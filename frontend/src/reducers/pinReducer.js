import {
    ERROR,
    FETCH_ALL_PINS,
    FETCH_ALL_PINS_ERROR,
    FETCH_ALL_PINS_SUCCESS,
    FETCH_CREATE_PIN,
    FETCH_CREATE_PIN_ERROR,
    FETCH_CREATE_PIN_SUCCESS,
    FETCH_DELETE_PIN,
    FETCH_DELETE_PIN_ERROR,
    FETCH_DELETE_PIN_SUCCESS,
    FETCH_LIKE_PIN,
    FETCH_LIKE_PIN_ERROR,
    FETCH_LIKE_PIN_SUCCESS,
    FETCH_LOAD_MORE_PINS, FETCH_LOAD_MORE_PINS_SUCCESS,
    FETCH_PIN_CATEGORIES,
    FETCH_PIN_CATEGORIES_ERROR,
    FETCH_PIN_CATEGORIES_SUCCESS,
    FETCH_PIN_DETAIL,
    FETCH_PIN_DETAIL_ERROR,
    FETCH_PIN_DETAIL_SUCCESS,
    FETCH_PINS_BY_CATEGORY,
    FETCH_PINS_BY_CATEGORY_ERROR,
    FETCH_PINS_BY_CATEGORY_SUCCESS, FETCH_UNLIKE_PIN, FETCH_UNLIKE_PIN_ERROR, FETCH_UNLIKE_PIN_SUCCESS,
    SUCCESS
} from "../actions/constants";

const initialState = {
    pins: [],
    totalPinsCount: 0,
    categories: [],
    isLoading: false,
    alert: {
        active: false,
        type: '',
        message: ''
    },
    pinDetail: null,
}

const pinReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ALL_PINS :
        case FETCH_PINS_BY_CATEGORY:
        case FETCH_PIN_CATEGORIES :
        case FETCH_PIN_DETAIL :
        case FETCH_CREATE_PIN :
        case FETCH_DELETE_PIN :
            return {
                ...state, isLoading: true,
                alert: {...initialState.alert}
            };

        case FETCH_LOAD_MORE_PINS :
            return {
                ...state,
                isLoading: false,
                alert: {...initialState.alert}
            };

        case FETCH_LIKE_PIN :
        case FETCH_UNLIKE_PIN :
            return {
                ...state,
                alert: {...initialState.alert}
            }

        case FETCH_ALL_PINS_ERROR:
        case FETCH_PINS_BY_CATEGORY_ERROR:
        case FETCH_PIN_CATEGORIES_ERROR :
        case FETCH_PIN_DETAIL_ERROR:
        case FETCH_CREATE_PIN_ERROR :
        case FETCH_DELETE_PIN_ERROR :
        case FETCH_LIKE_PIN_ERROR :
        case FETCH_UNLIKE_PIN_ERROR :
            return {
                ...state,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: 'Une erreur s\'est produite'
                }
            };

        case FETCH_ALL_PINS_SUCCESS:
        case FETCH_PINS_BY_CATEGORY_SUCCESS:
            return {
                ...state,
                totalPinsCount: action.totalPinsCount,
                isLoading: false,
                pins: action.data
            };

        case FETCH_LOAD_MORE_PINS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                pins: [...state.pins, ...action.data]
            };

        case FETCH_PIN_CATEGORIES_SUCCESS :
            return {
                ...state,
                isLoading: false,
                categories: action.data,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_PIN_DETAIL_SUCCESS:
            return {
                ...state,
                isLoading: false,
                pinDetail: action.data
            };

        case FETCH_CREATE_PIN_SUCCESS :
            return {
                ...state,
                pins: [...state.pins, action.data],
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Votre média a bien été ajouté'
                }
            }

        case FETCH_DELETE_PIN_SUCCESS :
            return {
                ...state,
                pins: state.pins.filter(item => item.id !== action.data.id),
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Votre média a bien été supprimé'
                }
            }

        case FETCH_LIKE_PIN_SUCCESS :
            return {
                ...state,
                pins: [
                    ...state.pins.map(item => {
                        if (item.id !== action.data.id) {
                            return item
                        } else {
                            return action.data
                        }
                    }),
                ],
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Le média a bien été ajouté à vos favoris'
                }
            }

        case FETCH_UNLIKE_PIN_SUCCESS :
            return {
                ...state,
                pins: [
                    ...state.pins.map(item => {
                        if (item.id !== action.data.id) {
                            return item
                        } else {
                            return action.data
                        }
                    }),
                ],
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Le média a bien été supprimé de vos favoris'
                }
            }


        default:
            return state;
    }
}

export default pinReducer;