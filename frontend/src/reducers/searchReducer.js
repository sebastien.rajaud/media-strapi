import {ERROR, FETCH_SEARCH, FETCH_SEARCH_ERROR, FETCH_SEARCH_SUCCESS} from "../actions/constants";

const initialState = {
    pins: [],
    isLoading: false,
    alert : {
        active:false,
        type: '',
        message: ''
    }
}


const searchReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_SEARCH :
            return {
                ...state, isLoading: true,
                alert : {...initialState.alert}
            }
        case FETCH_SEARCH_ERROR :
            return {
                ...state, isLoading: false,
                alert : {
                    active: true,
                    type: ERROR,
                    message: action.error
                }
            }

        case FETCH_SEARCH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                pins: action.data,
                alert: {...initialState.alert}
            }

        default:
            return state;
    }
}

export default searchReducer;