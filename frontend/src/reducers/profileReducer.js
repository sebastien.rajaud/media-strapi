import {
    ERROR, FETCH_LIKE_PIN, FETCH_LIKE_PIN_SUCCESS,
    FETCH_PINS_CREATES_BY_USER,
    FETCH_PINS_CREATES_BY_USER_ERROR,
    FETCH_PINS_CREATES_BY_USER_SUCCESS,
    FETCH_PINS_SAVES_BY_USER,
    FETCH_PINS_SAVES_BY_USER_ERROR,
    FETCH_PINS_SAVES_BY_USER_SUCCESS,
    FETCH_PROFILE,
    FETCH_PROFILE_ERROR,
    FETCH_PROFILE_SUCCESS, FETCH_UNLIKE_PIN, FETCH_UNLIKE_PIN_SUCCESS,
    FETCH_UPDATE_USER,
    FETCH_UPDATE_USER_ERROR,
    FETCH_UPDATE_USER_SUCCESS, SUCCESS,
} from "../actions/constants";

const initialState = {
    user: null,
    pins: [],
    isLoading: false,
    alert: {
        active: false,
        type: '',
        message: ''
    }
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PINS_CREATES_BY_USER:
        case FETCH_PROFILE :
        case FETCH_PINS_SAVES_BY_USER :
        case FETCH_UPDATE_USER :

            return {
                ...state,
                isLoading: true,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_LIKE_PIN :
        case FETCH_UNLIKE_PIN :
            return {
                ...state,
                alert: {...initialState.alert}
            }

        case FETCH_UNLIKE_PIN_SUCCESS : return {
            ...state,
            pins: [
                ...state.pins.filter(item => item.id !== action.data.id)
            ],
            isLoading: false,
            alert: {
                active: true,
                type: SUCCESS,
                message: 'Le média a bien été retiré à vos favoris'
            }
        }

        case FETCH_LIKE_PIN_SUCCESS :
            return {
                ...state,
                pins: [
                    ...state.pins.map(item => {
                        if (item.id !== action.data.id) {
                            return item
                        } else {
                            return action.data
                        }
                    }),
                ],
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Le média a bien été ajouté à vos favoris'
                }
            }

        case FETCH_PROFILE_SUCCESS :
            return {
                ...state,
                user: action.data,
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_UPDATE_USER_SUCCESS :
            return {
                ...state,
                user: action.data,
                isLoading: false,
                alert: {...initialState.alert}
            }
        case FETCH_PINS_CREATES_BY_USER_SUCCESS :
            return {
                ...state,
                pins: action.data,
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_PINS_SAVES_BY_USER_SUCCESS :
            return {
                ...state,
                pins: action.data,
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            }

        case FETCH_PINS_CREATES_BY_USER_ERROR :
        case FETCH_PROFILE_ERROR :
        case FETCH_PINS_SAVES_BY_USER_ERROR :
        case FETCH_UPDATE_USER_ERROR :
            return {
                ...state,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: 'Une erreur est survenue'
                }
            }
        default:
            return state;
    }
}

export default profileReducer;
