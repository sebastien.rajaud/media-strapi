import {combineReducers} from "redux";
import commentReducer from "./commentReducer";
import userReducer from "./userReducer";
import pinReducer from "./pinReducer";
import profileReducer from "./profileReducer";
import searchReducer from "./searchReducer";

 const reducers = combineReducers({
     commentReducer,
     userReducer,
     pinReducer,
     profileReducer,
     searchReducer
 })

export default reducers