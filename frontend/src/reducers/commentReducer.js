import {
    ERROR, FETCH_ADD_COMMENT, FETCH_ADD_COMMENT_ERROR, FETCH_ADD_COMMENT_SUCCESS,
    FETCH_ALL_COMMENTS,
    FETCH_ALL_COMMENTS_ERROR,
    FETCH_ALL_COMMENTS_SUCCESS, SUCCESS
} from "../actions/constants";

const initialState = {
    comments: [],
    isLoading: false,
    alert: {
        active: false,
        type: '',
        message: ''
    }
}

const commentReducer = (state = initialState, action) => {

    switch (action.type) {

        case FETCH_ALL_COMMENTS :
        case FETCH_ADD_COMMENT:
            return {
                ...state,
                isLoading: true
            };
        case FETCH_ALL_COMMENTS_SUCCESS :
            return {
                ...state,
                comments: [...action.data],
                isLoading: false,
                alert: {
                    active: false,
                    type: '',
                    message: ''
                }
            };
        case FETCH_ALL_COMMENTS_ERROR :
            return {
                ...state,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: action?.error
                }
            };
        case FETCH_ADD_COMMENT_SUCCESS :
            return {
                ...state,
                comments: [...state.comments, action.data],
                isLoading: false,
                alert: {
                    active: true,
                    type: SUCCESS,
                    message: 'Le commentaire a bien été ajouté'
                }
            }
        case FETCH_ADD_COMMENT_ERROR :
            return {
                ...state,
                isLoading: false,
                alert: {
                    active: true,
                    type: ERROR,
                    message: 'Une erreur est survenue lors de l\'ajout de votre commentaire'
                }
            }
        default :
            return state;
    }

}

export default commentReducer;