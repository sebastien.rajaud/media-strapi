import API, {LIMIT} from "./index";

/**
 * Fetch all pins
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPins = async (limit = LIMIT) => await API(`/pins?_limit=${limit}&_start=0`,);

/**
 * Fetch all pins for Load more button
 *
 * @param start
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPinsLoadMore = async (start) => await API(`/pins?_limit=${LIMIT}&_start=${start}`,);


/**
 * Fetch pins by category
 *
 * @param slug
 * @returns {AxiosPromise<any>}
 */
export const fetchPinsByCategory = async (slug, limit = LIMIT) => API(`/pins/category/${slug}?_limit=${limit}`);

/**
 * Fetch pins by category for Load More
 *
 * @param slug
 * @param limit
 * @returns {AxiosPromise<any>}
 */
export const fetchPinsByCategoryLoadMore = async (slug, start) => API(`/pins/category/${slug}?_limit=${LIMIT}&_start=${start}`);
/**
 * Fetch pin categories
 *
 * @param token
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPinCategories = async (token) => await API(`/categories`);


/**
 * Fetch Like
 *
 * @param pinId
 * @param userId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchLike = async (pinId, userId) => await API.put(`/pins/like/${pinId}`, {userId});

/**
 * Fetch unlike
 *
 * @param pinId
 * @param userId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchUnlikePin = async (pinId, userId) => await API.put(`/pins/unlike/${pinId}`, {userId});

/**
 * Fetch Delete Pin
 *
 * @param pinId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchDeletePin = async (pinId) => await API.delete(`/pins/${pinId}`);


/**
 * Fetch Create Pin
 *
 * @param formData
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchCreatePin = async (formData) => {
    return await API.post(`/pins/`, formData, {
        headers: {
            'Content-Type': 'mulitpart/form-data',
            'accept': 'application/json',
        }
    })
}

/**
 * Fetch Pin Detail
 *
 * @param pinId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPinDetail = async (pinId) => await API(`/pins/${pinId}`);

/**
 * Fetch create comment
 * @param formData
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchCreateComment = async (formData) => await API.post(`/comments`, formData, {
    headers: {
        'Content-Type': 'mulitpart/form-data',
        'accept': 'application/json',
    }
})

/**
 * Fetch comments by pin
 *
 * @param pinId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchCommentsByPin = async (pinId) => await API(`/comments/pin/${pinId}`)

/**
 * Fetch pins saves by user
 * @param userId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPinsSavesByUser = async (userId) => await API(`/pins/saves/${userId}`)

/**
 * Fetch pins create by user
 *
 * @param userId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchPinsCreatesByUser = async (userId) => await API(`/pins/creates/${userId}`)