import API from "./index";

/**
 * Fetch search
 * @param searchTerm
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchSearch = async (searchTerm) =>  await API(`/pins/search/${searchTerm}`,);