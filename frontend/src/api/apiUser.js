import API from '../api';
import jwtDecode from "jwt-decode";

/**
 * Fetch login
 *
 * @param email
 * @param password
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchLogin = async (email, password) => API.post(`${process.env.REACT_APP_STRAPI_BACKEND_URL}/auth/local`, {
    identifier: email,
    password
})

/**
 * Hydrate user when refresh page
 * @returns {Promise<boolean|AxiosResponse<*>>}
 */
export const fetchHydrateUser = async () => {
    const jwt = localStorage.getItem('sepanso_media_user');

    if (jwt) {
        const {id} = jwtDecode(jwt);
        return await fetchUserById(id);
    }
    return false;
}

/**
 * Guards check if user is authenticate
 *
 * @returns {boolean}
 */
export const isAuthenticated = () => {

    const jwt = JSON.parse(localStorage.getItem('sepanso_media_user'));

    if (jwt) {
        const {exp} = jwtDecode(jwt);
        if (exp * 1000 > new Date().getTime()) {
            API.defaults.headers['Authorization'] = `Bearer ${jwt}`;
            return true;
        }
        return false;
    }
    return false;

}

/**
 * Logout user - delete JWT from localstorage
 */
export const fetchlogout = () => {
    API.defaults.headers['Authorization'] = ``;
    localStorage.clear();
}

/**
 * Fetch user by id
 * @param userId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchUserById = async (userId) => await API(`/users/${userId}`);

/**
 * Fetch update user
 * @param userId
 * @param form
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchUpdateUser = async (userId, form) => await API.put(`/users/${userId}`, form);

/**
 * Fetch update avatar
 *
 * @param formData
 * @returns {Promise<AxiosResponse<any>>}
 */
export const fetchUpdateAvatarUser = async (formData) => await API.post(`/upload`, formData, {
    headers: {
        'Content-Type': 'mulitpart/form-data',
        'accept': 'application/json',
    }
})