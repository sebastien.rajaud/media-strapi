import axios from 'axios';

export const LIMIT = 10;

const API = axios.create({
    baseURL: process.env.REACT_APP_STRAPI_BACKEND_URL,
})

export default API;
