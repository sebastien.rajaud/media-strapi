export const getMediaUrl = (url) => {
    return `${process.env.REACT_APP_STRAPI_BACKEND_URL}${url}`
}

export const handleDownload = async (url, filename) => {
    try {

        const response = await fetch(url, {
            method: 'GET',
            headers: {
                "Origin": "http://localhost:3000"
            }
        })

        const imageBlob = await response.blob();

        const imageUrl = URL.createObjectURL(imageBlob)
        const link = document.createElement('a');
        link.href = imageUrl
        link.download = filename
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    } catch (e) {
        console.log(e)
    }
}

export const uploadImage = (e, setPreview, setError = null) => {

    const file = e.target.files[0];

    if (
        file && (
            file.type === 'image/jpeg' ||
            file.type === 'image/png' ||
            file.type === 'image/gif' ||
            file.type === 'image/tiff' ||
            file.type === 'image/svg'
        )
    ) {
        const fileReader = new FileReader();

        fileReader.onload = () => {
            setPreview(fileReader.result)
        }
        fileReader.readAsDataURL(file);


    } else {
        if (setError) {

            setError({
                isError: true,
                type: 'warning',
                message: 'Format available for images : jpeg, png, tiff, svg'
            })
        }
    }
}