export const getUser = () => {
    const user = localStorage.getItem('sepanso_media_user') !== 'undefined' ? JSON.parse(localStorage.getItem('sepanso_media_user')) : localStorage.clear();
    return user;
}

export const getFullname = (user) => {
    return `${user?.firstname} ${user?.lastname}`;
}