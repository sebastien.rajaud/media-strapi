export const generateFormData = (formElements) => {
    const formData = new FormData();
    const data = {};

    for (let i = 0; i < formElements.length; i++) {
        const currentElement = formElements[i];
        if (!['submit', 'file', 'button'].includes(currentElement.type)) {
            data[currentElement.name] = currentElement.value;
        } else if (currentElement.type === 'file') {
            for (let i = 0; i < currentElement.files.length; i++) {
                const file = currentElement.files[i];
                formData.append(`files.${currentElement.name}`, file, file.name);
            }
        }
    }
    formData.append('data', JSON.stringify(data));

    return formData;
}

export const jsonToFormData = (obj) => {
    const formData = new FormData();

    formData.append('data', JSON.stringify(obj));
    return formData;
}