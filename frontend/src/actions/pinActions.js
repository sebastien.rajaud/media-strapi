import {
    FETCH_ALL_PINS,
    FETCH_ALL_PINS_ERROR,
    FETCH_ALL_PINS_SUCCESS,
    FETCH_CREATE_PIN,
    FETCH_CREATE_PIN_ERROR,
    FETCH_CREATE_PIN_SUCCESS,
    FETCH_DELETE_PIN,
    FETCH_DELETE_PIN_ERROR,
    FETCH_DELETE_PIN_SUCCESS,
    FETCH_LIKE_PIN,
    FETCH_LIKE_PIN_ERROR,
    FETCH_LIKE_PIN_SUCCESS, FETCH_LOAD_MORE_PINS, FETCH_LOAD_MORE_PINS_ERROR, FETCH_LOAD_MORE_PINS_SUCCESS,
    FETCH_PIN_CATEGORIES,
    FETCH_PIN_CATEGORIES_ERROR,
    FETCH_PIN_CATEGORIES_SUCCESS,
    FETCH_PIN_DETAIL,
    FETCH_PIN_DETAIL_ERROR,
    FETCH_PIN_DETAIL_SUCCESS,
    FETCH_PINS_BY_CATEGORY,
    FETCH_PINS_BY_CATEGORY_ERROR,
    FETCH_PINS_BY_CATEGORY_SUCCESS, FETCH_UNLIKE_PIN, FETCH_UNLIKE_PIN_ERROR, FETCH_UNLIKE_PIN_SUCCESS
} from "./constants";
import * as API from "../api/apiPins";

export const fetchAllPins = () => async dispatch => {
    dispatch({
        type: FETCH_ALL_PINS
    })
    try {
        const {data} = await API.fetchPins()
        const totalPinsCount = await API.fetchPins(-1);

        dispatch({
            type: FETCH_ALL_PINS_SUCCESS,
            data,
            totalPinsCount: totalPinsCount.data.length
        })
    } catch (e) {
        dispatch({
            type: FETCH_ALL_PINS_ERROR
        })
    }
}

export const fetchAllPinsLoadMore = (start) => async dispatch => {
    dispatch({
        type: FETCH_LOAD_MORE_PINS
    })
    try {
        const {data} = await API.fetchPinsLoadMore(start)

        return dispatch({
            type: FETCH_LOAD_MORE_PINS_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_LOAD_MORE_PINS_ERROR
        })
    }
}

export const fetchPinsByCategory = (slug) => async dispatch => {
    dispatch({
        type: FETCH_PINS_BY_CATEGORY
    })
    try {
        const {data} = await API.fetchPinsByCategory(slug)
        const totalPinsCount = await API.fetchPinsByCategory(slug, -1);
        return dispatch({
            type: FETCH_PINS_BY_CATEGORY_SUCCESS,
            data,
            totalPinsCount: totalPinsCount.data.length
        })
    } catch (e) {
        dispatch({
            type: FETCH_LOAD_MORE_PINS_ERROR
        })
    }
}

export const fetchPinsByCategoryLoadMore = (slug, start) => async dispatch => {
    dispatch({
        type: FETCH_LOAD_MORE_PINS
    })
    try {
        const {data} = await API.fetchPinsByCategoryLoadMore(slug, start)

        dispatch({
            type: FETCH_LOAD_MORE_PINS_SUCCESS,
            data,
        })
    } catch (e) {
        dispatch({
            type: FETCH_PINS_BY_CATEGORY_ERROR
        })
    }
}

export const fetchPinCategories = () => async dispatch => {
    dispatch({
        type: FETCH_PIN_CATEGORIES
    })

    try {
        const {data} = await API.fetchPinCategories();
        dispatch({
            type: FETCH_PIN_CATEGORIES_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_PIN_CATEGORIES_ERROR
        })
    }
}

export const fetchPinDetail = (pinId) => async dispatch => {
    dispatch({
        type: FETCH_PIN_DETAIL
    })
    try {
        const {data} = await API.fetchPinDetail(pinId)
        dispatch({
            type: FETCH_PIN_DETAIL_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_PIN_DETAIL_ERROR,
            error: e?.response?.data
        })
    }
}

export const fetchCreatePin = (formData) => async dispatch => {
    dispatch({
        type: FETCH_CREATE_PIN
    })
    try {
        const {data} = await API.fetchCreatePin(formData)
        dispatch({
            type: FETCH_CREATE_PIN_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_CREATE_PIN_ERROR,
            error: e
        })
    }
}

export const fetchDeletePin = (pinId) => async dispatch => {
    dispatch({
        type: FETCH_DELETE_PIN
    })
    try {
        const {data} = await API.fetchDeletePin(pinId);
        dispatch({
            type: FETCH_DELETE_PIN_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_DELETE_PIN_ERROR,
            error: e
        })
    }
}

export const fetchLikePin = (pinId, userId) => async dispatch => {
    dispatch({
        type: FETCH_LIKE_PIN
    })
    try {
        const {data} = await API.fetchLike(pinId, userId);
        console.log(data)
        dispatch({
            type: FETCH_LIKE_PIN_SUCCESS,
            data,
            userId
        })
        return data;
    } catch (e) {
        console.log(e)
        dispatch({
            type: FETCH_LIKE_PIN_ERROR,
            error: e
        })
    }
}


export const fetchUnlikePin = (pinId, userId) => async dispatch => {
    dispatch({
        type: FETCH_UNLIKE_PIN
    })
    try {
        const {data} = await API.fetchUnlikePin(pinId, userId);
        console.log(data)
        dispatch({
            type: FETCH_UNLIKE_PIN_SUCCESS,
            data,
            userId
        })
        return data;
    } catch (e) {
        console.log(e)
        dispatch({
            type: FETCH_UNLIKE_PIN_ERROR,
            error: e
        })
    }
}