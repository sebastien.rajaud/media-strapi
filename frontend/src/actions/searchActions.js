import {FETCH_SEARCH, FETCH_SEARCH_ERROR, FETCH_SEARCH_SUCCESS} from "./constants";
import * as API from '../api/apiSearch';

export const fetchSearch = (searchTerm) => async dispatch => {
    dispatch({
        type: FETCH_SEARCH
    })
    try {
        const {data} = await API.fetchSearch(searchTerm)
        dispatch({
            type: FETCH_SEARCH_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_SEARCH_ERROR
        })
    }
}