import {
    FETCH_PINS_CREATES_BY_USER,
    FETCH_PINS_CREATES_BY_USER_ERROR,
    FETCH_PINS_CREATES_BY_USER_SUCCESS,
    FETCH_PINS_SAVES_BY_USER, FETCH_PINS_SAVES_BY_USER_ERROR,
    FETCH_PINS_SAVES_BY_USER_SUCCESS,
    FETCH_PROFILE,
    FETCH_PROFILE_ERROR,
    FETCH_PROFILE_SUCCESS, FETCH_UPDATE_USER, FETCH_UPDATE_USER_ERROR, FETCH_UPDATE_USER_SUCCESS
} from "./constants";
import * as API_USER from "../api/apiUser";
import * as API_PIN from "../api/apiPins";
import jwtDecode from "jwt-decode";

export const fetchProfile = (id) => async dispatch => {
    dispatch({
        type: FETCH_PROFILE
    })
    try {
        const {data} = await API_USER.fetchUserById(id);
        dispatch({
            type: FETCH_PROFILE_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_PROFILE_ERROR
        })
    }
}


const checkIfUserIsAllowedToUpdate = (userId) => {
    const jwt = localStorage.getItem(process.env.REACT_APP_JWT_NAME);
    if (jwt) {
        const {id} = jwtDecode(jwt);
        if (id === userId) {
            return true;
        }
        return false;
    }
    return false;
}

/**
 * Fetch update user
 * @param userId
 * @param udpateObject
 * @returns {(function(*): Promise<void>)|*}
 */
export const fetchUpdateUser = (userId, udpateObject) => async dispatch => {
    dispatch({
        type: FETCH_UPDATE_USER
    })
    try {
        if (checkIfUserIsAllowedToUpdate(userId)) {
            const {data} = await API_USER.fetchUpdateUser(userId, udpateObject);
            dispatch({
                type: FETCH_UPDATE_USER_SUCCESS,
                data
            })
        } else {
            throw new Error('Vous n\'êtes pas authorisé à mettre à jour ce profil.')
        }

    } catch (e) {
        dispatch({
            type: FETCH_UPDATE_USER_ERROR,
            error: e
        })
    }
}

export const fetchUpdateAvatarUser = (formData) => async dispatch => {
    dispatch({
        type: FETCH_UPDATE_USER
    })
    try {
        if (checkIfUserIsAllowedToUpdate(JSON.parse(formData.get('data')).refId)) {
            const {data} = await API_USER.fetchUpdateAvatarUser(formData)


            return data;
        } else {
            throw new Error('Vous n\'êtes pas authorisé à mettre à jour ce profile')
        }
    } catch (e) {
        dispatch({
            type: FETCH_UPDATE_USER_ERROR,
            error: e
        })
    }
}

/**
 * Fetch pins creates by user
 * @param id
 * @returns {(function(*): Promise<void>)|*}
 */
export const fetchPinsCreatesByUser = (id) => async dispatch => {
    dispatch({
        type: FETCH_PINS_CREATES_BY_USER
    })
    try {
        const {data} = await API_PIN.fetchPinsCreatesByUser(id);
        dispatch({
            type: FETCH_PINS_CREATES_BY_USER_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_PINS_CREATES_BY_USER_ERROR,
            error: e
        })
    }
}

/**
 * Fetch pins saves by user
 * @param id
 * @returns {(function(*): Promise<void>)|*}
 */
export const fetchPinsSavesByUser = (id) => async dispatch => {
    dispatch({
        type: FETCH_PINS_SAVES_BY_USER
    })
    try {
        const {data} = await API_PIN.fetchPinsSavesByUser(id);
        dispatch({
            type: FETCH_PINS_SAVES_BY_USER_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_PINS_SAVES_BY_USER_ERROR,
            error: e
        })
    }
}