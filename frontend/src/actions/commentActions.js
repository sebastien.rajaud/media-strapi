import {
    FETCH_ADD_COMMENT, FETCH_ADD_COMMENT_ERROR, FETCH_ADD_COMMENT_SUCCESS,
    FETCH_ALL_COMMENTS,
    FETCH_ALL_COMMENTS_ERROR,
    FETCH_ALL_COMMENTS_SUCCESS
} from "./constants";
import {fetchCommentsByPin, fetchCreateComment} from "../api/apiPins";

export const fetchAllComments = (id) => async (dispatch) => {
    dispatch({
        type: FETCH_ALL_COMMENTS
    });
    try {
        const {data} = await fetchCommentsByPin(id);
        dispatch({
            type: FETCH_ALL_COMMENTS_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_ALL_COMMENTS_ERROR,
            error: e?.response.data
        })
        console.log(e)
    }

}


export const fetchAddComment = (formData) => async dispatch => {
    dispatch({
        type: FETCH_ADD_COMMENT
    })
    try {
        const {data} = await fetchCreateComment(formData);
        dispatch({
            type: FETCH_ADD_COMMENT_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: FETCH_ADD_COMMENT_ERROR,
            error: e?.response.data
        })
    }
}