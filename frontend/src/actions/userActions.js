import {
    HYDRATE_USER,
    HYDRATE_USER_ERROR, HYDRATE_USER_SUCCESS,
    LOGIN,
    LOGIN_ERROR,
    LOGIN_SUCCESS,
    LOGOUT,
    LOGOUT_ERROR,
    LOGOUT_SUCCESS
} from "./constants";
import {fetchHydrateUser, fetchLogin, fetchlogout} from '../api/apiUser';
import API from "../api";

export const login = (email, password, navigate) => async dispatch => {
    dispatch({
        type: LOGIN
    })
    try {
        if (!email || !password) {
            throw new Error('Un des champs requis est manquant')
        }
        const {data} = await fetchLogin(email, password);

        if (data) {
            const {jwt, user} = data;

            dispatch({
                type: LOGIN_SUCCESS,
                data: user
            })
            localStorage.setItem('sepanso_media_user', JSON.stringify(data.jwt));
            API.defaults.headers['Authorization'] = `Bearer ${jwt}`;
            navigate('/');
            return true;
        }


    } catch (e) {
        const message = e?.response?.data.statusCode === 400 ? 'Les identifiants sont invalides' : e.message
        dispatch({
            type: LOGIN_ERROR,
            error: message
        })
    }
}

export const logout = (navigate) => dispatch => {
    dispatch({
        type: LOGOUT
    })
    try {
        fetchlogout();
        dispatch({
            type: LOGOUT_SUCCESS
        })
        navigate('/login');
    } catch (e) {
        dispatch({
            type: LOGOUT_ERROR,
            error: e
        })
    }
}

export const hydrateUser = () => async dispatch => {
    dispatch({
        type: HYDRATE_USER
    })
    try {
        const {data} = await fetchHydrateUser();
        dispatch({
            type: HYDRATE_USER_SUCCESS,
            data
        })
    } catch (e) {
        dispatch({
            type: HYDRATE_USER_ERROR,
            error: e?.response.data
        })
    }
}