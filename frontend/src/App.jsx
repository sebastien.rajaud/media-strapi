import React from "react";
import {Routes, Route} from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import Auth from './components/Auth/Auth';
import Home from './container/Home';


const App = () => {



    return (
        <Routes>
            <Route path="login" element={<Auth/>}/>
            <Route path="/*" element={
                <PrivateRoute>
                    <Home/>
                </PrivateRoute>
            }
            />
        </Routes>
    )
}

export default App;