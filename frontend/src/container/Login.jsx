import React from 'react';

import {useNavigate} from "react-router-dom";
import {login} from "../actions/userActions";
import LoginForm from "../components/Auth/LoginForm";
import {useDispatch, useSelector} from "react-redux";
import Alert from "../components/Alert";

const Login = () => {
    const alert = useSelector(state => state.userReducer.alert);
    const dispatch = useDispatch();
    const navigate = useNavigate();



    const handleSubmit = async (email, password) => {
        try {
            return await dispatch(login(email, password, navigate));
        } catch (e) {
            console.log(e)
        }
    }

    return (
        <div className="mt-4 w-300">
            <LoginForm handleSubmit={handleSubmit}/>
            {alert.active && <Alert type={alert.type} message={alert.message}/>}
        </div>
    );
}

export default Login;