import React, {useState} from 'react';
import {Routes, Route} from 'react-router-dom';
import Feed from "../components/Pin/Feed";
import PinDetail from "../components/Pin/PinDetail";
import Navbar from "../components/Navbar";
import Search from "../components/Search/Search";

function Pins({user}) {

    const [searchTerm, setSearchTerm] = useState('');

    return (
        <div className="px-2 md:px-5">
            <div className="bg-gray-50">
                <Navbar searchTerm={searchTerm} setSearchTerm={setSearchTerm} user={user}/>
            </div>
            <div className="h-full">
                <Routes>
                    <Route path="/" element={<Feed/>}/>
                    <Route path="/category/:slug" element={<Feed/>}/>
                    <Route path="/pin-detail/:pinId" element={<PinDetail user={user}/>}/>
                    <Route path="/search" element={<Search searchTerm={searchTerm} />}/>

                </Routes>
            </div>
        </div>
    );
}

export default Pins;