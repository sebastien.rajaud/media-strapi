import React, {useState} from 'react';
import {jsonToFormData} from "../utils/forms";
import {useDispatch, useSelector} from "react-redux";
import {fetchAddComment} from "../actions/commentActions";

const AddCommentForm = ({id}) => {

    const user = useSelector(state => state.userReducer.user);

    const dispatch = useDispatch();
    const isLoading = useSelector(state => state.commentReducer.isLoading)

    const [comment, setComment] = useState();

    /**
     * Handle Comment form submission
     * @param e
     * @returns {Promise<void>}
     */
    const handleComment = async (e) => {
        e.preventDefault();

        try {
            const data = jsonToFormData({
                pin: id,
                comment,
                postedBy: user.id
            })
            dispatch(fetchAddComment(data))
            setComment('');

        } catch (e) {
            console.log(e)
        }
    }

    return (
        <div>
            <div className="flex flex-col mt-6 gap-3">
                <form>
                    <label
                        htmlFor="comment"
                        className="block text-sm font-medium text-gray-700"
                    >Add comment</label>
                    <textarea
                        rows={5}
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                        className="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300 mt-2"
                    />
                    <div className="flex justify-end mt-5">
                        <button
                            type="button"
                            className="bg-indigo-500 text-white font-bold p-2 rounded-md w-50 outline-none"
                            disabled={!comment || comment === '' || isLoading}
                            onClick={handleComment}
                        >
                            {isLoading ? 'Loading' : 'Add comment'}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default AddCommentForm;