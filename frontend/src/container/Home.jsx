import React, {useState, useEffect, useRef} from 'react';
import Sidebar from "../components/Sidebar";
import {Link, Route, Routes} from "react-router-dom";
import UserProfile from "../components/UserProfile/UserProfile";
import Pins from "../container/Pins";
import {HiMenu} from "react-icons/hi";
import {AiFillCloseCircle} from "react-icons/ai";
import logo from "../assets/logo.png";
import {useDispatch, useSelector} from "react-redux";
import {hydrateUser} from "../actions/userActions";
import Alert from "../components/Alert";

const Home = () => {
    const [toggleSidebar, setToggleSidebar] = useState(false);
    const scrollRef = useRef(null);
    const user = useSelector(state => state.userReducer.user);
    const alertUser = useSelector(state => state.userReducer.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        if(!user) {
            dispatch(hydrateUser());
        }
    }, [dispatch, user])


    return (
        <div className="flex bg-gray-50 md:flex-row flex-col h-screen transaction-height duration-75 ease-out">
            <div className="hidden md:flex h-screen flex-initial">
                <Sidebar user={user && user} />
            </div>
            <div className="flex md:hidden flex-row">
                <div className="p-2 w-full flex flex-row justify-between items-center shadow-md">
                    <HiMenu fontSize={40} className="cursor-pointer" onClick={() => setToggleSidebar(true)}/>
                    <Link to="/">
                        <img src={logo} alt="logo" className="w-28"/>
                    </Link>
                    <Link to={`user-profile/${user?._id}`}>
                        <img src={user?.image} alt="logo" className="w-28"/>
                    </Link>
                </div>
                {toggleSidebar && (
                    <div className="fixed w-4/5 bg-white h-screen overflow-y-auto shadow-md z-10 animate-slide-in">
                        <div className="absolute w-full flex justify-end items-center p-2">
                            <AiFillCloseCircle fontSize={30} className="cursor-pointer"
                                               onClick={() => setToggleSidebar(false)}/>
                        </div>
                        <Sidebar user={user && user} closeToggle={setToggleSidebar}/>
                    </div>
                )}
            </div>
            <div className="pb-2 flex-1 h-screen overflow-y-scroll" ref={scrollRef}>
                <Routes>
                    <Route path="/user-profile/:userId" element={<UserProfile/>}/>
                    <Route path="/*" element={<Pins user={user && user}/>}/>
                </Routes>
            </div>
            {alertUser.active && <Alert type={alertUser.type} message={alertUser.message}/>}
        </div>
    );
};

export default Home;