import React, { useEffect} from 'react';
import Comment from "../components/Comment/Comment";
import AddCommentForm from "./AddCommentForm";
import {fetchAllComments} from "../actions/commentActions";
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../components/Spinner";
import Alert from "../components/Alert";

const Comments = ({id}) => {

    const dispatch = useDispatch();
    const allComments = useSelector(state => state.commentReducer.comments);
    const isLoading = useSelector(state => state.commentReducer.isLoading);
    const alert = useSelector(state => state.commentReducer.alert);

    useEffect(() => {
        dispatch(fetchAllComments(id))
    }, [dispatch,id])

    if (isLoading) return <Spinner message="Comments is loading..."/>;

    if (!allComments && allComments.length === 0) return null;

    return (
        <div>
            <h2 className="mt-5 text-xl text-indigo-500 uppercase font-bold">Comments</h2>
            <div className="flex flex-col mt-6 gap-3">
                {allComments.map(comment => <Comment key={comment.id} data={comment}/>)}
                <AddCommentForm id={id}/>
            </div>
            {alert.active && <Alert message={alert.message} type={alert.type} />}
        </div>
    );
}

export default Comments;