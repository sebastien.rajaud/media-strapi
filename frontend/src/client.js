import StrapiClient from 'strapi-client';
export const strapi = new StrapiClient(process.env.REACT_APP_STRAPI_BACKEND_URL)
