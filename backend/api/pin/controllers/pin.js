'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {parseMultipartData, sanitizeEntity} = require('strapi-utils');

module.exports = {

  async like(ctx) {
    const {id} = ctx.params;

    const data = ctx.request.body;
    const entity = await strapi.services.pin.like({id}, data.userId);

    return sanitizeEntity(entity, {model: strapi.models.pin})
  },

  async unlike(ctx) {
    const {id} = ctx.params;

    const data = ctx.request.body;
    const entity = await strapi.services.pin.unlike({id}, data.userId);

    return sanitizeEntity(entity, {model: strapi.models.pin})
  },

  async findByCategory(ctx) {
    let paramString = ctx.request.url;
    paramString = paramString.split('?')[1]
    const searchParams = new URLSearchParams(paramString);

    const limit = searchParams.has('_limit') ? searchParams.get('_limit') : -1;
    const start = searchParams.has('_start') ? searchParams.get('_start') : 0;

    const {slug} = ctx.params;
    const entities = await strapi.services.pin.findByCategory({slug, limit, start});

    return entities;

  },

  async search(ctx) {

    const {searchTerm} = ctx.params;
    try {
      const results = await strapi.services.pin.search(searchTerm);
      return results;

    } catch (e) {
      console.log(e)
    }

  },

  async findPinsSavesByUser(ctx) {

    const {userId} = ctx.params
    try {
      const entities = await strapi.services.pin.findPinsSavesByUser(userId)
      return entities;
    } catch (e) {
      console.log(e)
    }

  },

  async findPinsCreatesByUser(ctx) {

    const {userId} = ctx.params
    try {
      const entities = await strapi.services.pin.findPinsCreatesByUser(userId)
      return entities;
    } catch (e) {
      console.log(e)
    }

  },


}
