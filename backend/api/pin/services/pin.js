'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */
const {isDraft} = require('strapi-utils').contentTypes;

module.exports = {

  async like(params, userId) {

    try {

      const existingEntry = await strapi.query('pin').findOne(params);
      const userToAdd = await strapi.query('user', 'users-permissions').findOne({_id: userId});

      if (userToAdd) {

        let likes = null;
        likes = [...existingEntry.saves]
        likes.push(userToAdd);

        const validData = await strapi.entityValidator.validateEntityUpdate(
          strapi.models.pin,
          {saves: likes},
          {isDraft: isDraft(existingEntry, strapi.models.pin)}
        )

        const entry = await strapi.query('pin').update(params, validData);

        return entry;

      }
    } catch (e) {
      console.log(e)
    }

  },

  async unlike(params, userId) {
    try {
      const existingEntry = await strapi.query('pin').findOne(params);
      const userToRemove = await strapi.query('user', 'users-permissions').findOne({_id: userId});

      if (userToRemove) {
        let likes = null;

        likes = [...existingEntry.saves]
        likes = likes.filter(item => item.id.toString() !== userId);

        const validData = await strapi.entityValidator.validateEntityUpdate(
          strapi.models.pin,
          {saves: likes},
          {isDraft: isDraft(existingEntry, strapi.models.pin)}
        )

        const entry = await strapi.query('pin').update(params, validData);

        return entry;
      }

    } catch (e) {
      console.log(e)
    }
  },

  async findByCategory(params) {
    const {slug, limit, start} = params;
    try {

      const entities = await strapi.query('pin').find({'category.slug': slug, _limit: limit, _start: start});
      return entities;

    } catch (e) {
      console.log(e)
    }

  },

  async search(searchTerm) {

    try {
      const results = await strapi.query('pin').search({_q: searchTerm, _limit: 30})
      return results;
    } catch (e) {
      console.log(e);
    }

  },

  async findPinsSavesByUser(userId) {

    try {
      const results = await strapi.query('pin').find({saves: userId});
      return results;
    } catch (e) {
      console.log(e)
    }

  },

  async findPinsCreatesByUser(userId) {

    try {
      const results = await strapi.query('pin').find({postedBy: userId});
      return results;
    } catch (e) {
      console.log(e)
    }

  }


};
