'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */
const {isDraft} = require('strapi-utils').contentTypes;

module.exports = {

  /**
   *
   * @param body
   * @returns {Promise<*>}
   */
  async create(data) {

    const validData = await strapi.entityValidator.validateEntityCreation(
      strapi.models.comment,
      data,
      {isDraft: isDraft(data, strapi.models.comment)}
    );

    try {
      const entry = await strapi.query('comment').create(validData);
      return entry;
    } catch (e) {
      console.log(e)
    }

  },

  async findCommentsByPin(pinId) {
    try {
      const entries = await strapi.query('comment').find({pin: pinId})
      return entries;
    } catch (e) {
      console.log(e)
    }
  }

};
