'use strict';

const {sanitizeEntity, parseMultipartData} = require("strapi-utils");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {isDraft} = require('strapi-utils').contentTypes;

module.exports = {
  /**
   *
   * @param ctx
   * @returns {Promise<void>}
   */
  async create(ctx) {
    const {data} = parseMultipartData(ctx);
    const entity = await strapi.services.comment.create(data)

    return sanitizeEntity(entity, {model: strapi.models.comment});
  },

  async findCommentsByPin(ctx) {
    const {pinId} = ctx.params;
    const entities = await strapi.services.comment.findCommentsByPin(pinId);

    return entities.map(entity => sanitizeEntity(entity, {model: strapi.models.comment}))
  }

};
